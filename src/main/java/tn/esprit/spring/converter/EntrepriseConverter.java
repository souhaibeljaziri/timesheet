package tn.esprit.spring.converter;

import org.springframework.stereotype.Component;

import tn.esprit.spring.dto.EntrepriseDto;
import tn.esprit.spring.entities.Entreprise;

@Component
public class EntrepriseConverter {
	
	public Entreprise dtoToEntity(EntrepriseDto dto) {
		Entreprise entreprise = new Entreprise();
		entreprise.setDepartements(dto.getDepartements());
		entreprise.setName(dto.getName());
		entreprise.setRaisonSocial(dto.getRaisonSocial());
		entreprise.setId(dto.getId());
		return entreprise;
	}
}
