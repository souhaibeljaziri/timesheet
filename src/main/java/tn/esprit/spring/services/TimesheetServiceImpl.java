package tn.esprit.spring.services;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.esprit.spring.entities.*;
import tn.esprit.spring.repository.DepartementRepository;
import tn.esprit.spring.repository.EmployeRepository;
import tn.esprit.spring.repository.MissionRepository;
import tn.esprit.spring.repository.TimesheetRepository;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class TimesheetServiceImpl implements ITimesheetService {
	private static final Logger l = LogManager.getLogger(TimesheetServiceImpl.class);

	@Autowired
	MissionRepository missionRepository;
	@Autowired
	DepartementRepository deptRepoistory;
	@Autowired
	TimesheetRepository timesheetRepository;
	@Autowired
	EmployeRepository employeRepository;
	
	public int ajouterMission(Mission mission) {
		l.info(MessageFormat.format("Mission: {0}", mission));
		missionRepository.save(mission);
		l.debug(MessageFormat.format("Saved Mission: {0}", mission));
		l.debug(MessageFormat.format("Mission ID: {0}", mission.getId()));
		return mission.getId();
	}
    
	public Mission affecterMissionADepartement(int missionId, int depId) {
		l.info(MessageFormat.format("Mission ID: {0}", missionId));
        l.info(MessageFormat.format("Departement ID: {0}", depId));
		Mission mission = null;
		Departement dep = null;
		Optional<Mission> missionR = missionRepository.findById(missionId);
		Optional<Departement> depR = deptRepoistory.findById(depId);
		if (missionR.isPresent()) {
			mission = missionR.get();
			l.debug(MessageFormat.format("Fetched Mission: {0}", missionId));
        }		
		if (depR.isPresent()) {
			dep = depR.get();
			l.debug(MessageFormat.format("Fetched Departement: {0}", depId));
        }
		if(dep != null && mission != null)
		{
			mission.setDepartement(dep);
			l.info(MessageFormat.format("Add new Departement To Mission: {0}", mission));
		}
		
        missionRepository.save(mission);
        l.info(MessageFormat.format("Saved Mission: {0}", mission));

        return mission;
		
	}

	public Timesheet ajouterTimesheet(int missionId, int employeId, Date dateDebut, Date dateFin) {
		TimesheetPK timesheetPK = new TimesheetPK();
		timesheetPK.setDateDebut(dateDebut);
		timesheetPK.setDateFin(dateFin);
		timesheetPK.setIdEmploye(employeId);
		timesheetPK.setIdMission(missionId);
        l.info(MessageFormat.format("Timesheet Primary Key: {0}", timesheetPK));
		Timesheet timesheet = new Timesheet();
		timesheet.setTimesheetPK(timesheetPK);
		timesheet.setValide(false); //par defaut non valide
		timesheetRepository.save(timesheet);
		l.info(MessageFormat.format("Timesheet Saved or Updated if existed: {0}", timesheet));
		return timesheet;
		
		
	}

	
	public Timesheet validerTimesheet(int missionId, int employeId, Date dateDebut, Date dateFin, int validateurId) {
		l.info("In valider Timesheet");
		Employe validateur = null;
		Mission mission = null;
		Optional<Employe> validateurR = employeRepository.findById(validateurId);
		if (validateurR.isPresent()) {
			validateur = validateurR.get();
        }
		
		Optional<Mission> missionR = missionRepository.findById(missionId);
		if (missionR.isPresent()) {
			mission = missionR.get();
        }		
		//verifier s'il est un chef de departement (interet des enum)
		if(validateur != null && !validateur.getRole().equals(Role.CHEF_DEPARTEMENT))
		{
            l.debug(MessageFormat.format("l''employe doit etre chef de departement pour valider une feuille de temps !{0}", validateur));
			return null;
		}
		
		//verifier s'il est le chef de departement de la mission en question
		boolean chefDeLaMission = false;
		if(validateur != null)
		{
		for(Departement dep : validateur.getDepartements()){
			if(mission != null && dep.getId() == mission.getDepartement().getId()){
				chefDeLaMission = true;
				break;
			}
		}}
		if(!chefDeLaMission){
            l.debug(MessageFormat.format("l''employe doit etre chef de departement de la mission en question{0}", false));
			return null;
		}
		l.info("tout les condition sont vérifier");
		TimesheetPK timesheetPK = new TimesheetPK(missionId, employeId, dateDebut, dateFin);
		Timesheet timesheet =timesheetRepository.findBytimesheetPK(timesheetPK);
		timesheet.setValide(true);
				
		
		//Comment Lire une date de la base de données
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        l.debug(MessageFormat.format("dateDebut : {0}", dateFormat.format(timesheet.getTimesheetPK().getDateDebut())));
		l.info("Timesheet Valider: "); 
		timesheetRepository.save(timesheet);
		return timesheet;
		
	}

	
	public List<Mission> findAllMissionByEmployeJPQL(int employeId) {
		l.info(MessageFormat.format("All Mission By Employe {0}", timesheetRepository.findAllMissionByEmployeJPQL(employeId)));
		return timesheetRepository.findAllMissionByEmployeJPQL(employeId);
		
	}

	
	public List<Employe> getAllEmployeByMission(int missionId) {
        l.info(MessageFormat.format("All Employe By Mission {0}", timesheetRepository.getAllEmployeByMission(missionId)));
		return timesheetRepository.getAllEmployeByMission(missionId);
	}

}