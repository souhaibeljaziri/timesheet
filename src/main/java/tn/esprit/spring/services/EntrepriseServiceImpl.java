package tn.esprit.spring.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.entities.Entreprise;
import tn.esprit.spring.repository.DepartementRepository;
import tn.esprit.spring.repository.EntrepriseRepository;

@Service
public class EntrepriseServiceImpl implements IEntrepriseService {

	private static final Logger logger = LogManager.getLogger(EntrepriseServiceImpl.class);
	private static final String ENTRDEB = "Entreprise";

	@Autowired
	EntrepriseRepository entrepriseRepoistory;
	@Autowired
	DepartementRepository deptRepoistory;

	public int ajouterEntreprise(Entreprise entreprise) {
		logger.info("In ajouterEntreprise : ");
		logger.debug(ENTRDEB, entreprise);
		try {
			entrepriseRepoistory.save(entreprise);
			logger.info("Out ajouterEntreprise() without errors.");
			return entreprise.getId();
		} catch (Exception e) {
			logger.error("Erreur dans ajouterEntreprise() : ", e);
			return 0;
		}
	}

	public int ajouterDepartement(Departement dep) {
		logger.info("In ajouterDepartement() : ");
		logger.debug("Departement : {}", dep);
		try {
			deptRepoistory.save(dep);
			logger.info("Out ajouterDepartement() without errors.");
			return dep.getId();
		} catch (Exception e) {
			logger.error("Erreur dans ajouterDepartement() : ", e);
			return 0;
		}

	}

	public void affecterDepartementAEntreprise(int depId, int entrepriseId) {
		logger.info("In affecterDepartementAEntreprise() : ");
		logger.debug("DepartementId : %s , entrepriseId : %s", depId, entrepriseId);
		try {
			Optional<Entreprise> entrepriseManagedEntity = entrepriseRepoistory.findById(entrepriseId);
			logger.debug("entrepriseManagedEntity = {}", entrepriseManagedEntity);
			if (entrepriseManagedEntity.isPresent()) {
				Optional<Departement> depManagedEntity = deptRepoistory.findById(depId);
				logger.debug("depManagedEntity : {} ", depManagedEntity);
				if (depManagedEntity.isPresent()) {
					Departement depMan = depManagedEntity.get();
					depMan.setEntreprise(entrepriseManagedEntity.get());
					deptRepoistory.save(depMan);
				}
			}
			logger.info("Out affecterDepartementAEntreprise() without errors.");
		} catch (Exception e) {
			logger.error("Erreur dans affecterDepartementAEntreprise() : ", e);
		}
	}

	public List<String> getAllDepartementsNamesByEntreprise(int entrepriseId) {
		logger.info("In getAllDepartementsNamesByEntreprise() : ");
		logger.debug("EntrepriseId : {}", entrepriseId);
		try {
			Optional<Entreprise> entrepriseManagedEntity = entrepriseRepoistory.findById(entrepriseId);
			logger.debug("entrepriseManagedEntity : {} ", entrepriseManagedEntity);
			if (entrepriseManagedEntity.isPresent()) {
				List<String> depNames = new ArrayList<>();
				for (Departement dep : entrepriseManagedEntity.get().getDepartements()) {
					depNames.add(dep.getName());
				}
				logger.info("Out getAllDepartementsNamesByEntreprise() without list of departments.");
				return depNames;
			}
			logger.info("Out getAllDepartementsNamesByEntreprise() without emty list.");
			return Collections.emptyList();
		} catch (Exception e) {
			logger.error("Erreur dans getAllDepartementsNamesByEntreprise() : ", e);
			return Collections.emptyList();
		}
	}

	@Transactional
	public void deleteEntrepriseById(int entrepriseId) {
		logger.info("In deleteEntrepriseById() : ");
		logger.debug("EntrepriseId : {}", entrepriseId);
		try {
			Optional<Entreprise> ent = entrepriseRepoistory.findById(entrepriseId);
			logger.debug(ENTRDEB, ent);
			if (ent.isPresent()) {
				entrepriseRepoistory.delete(ent.get());
			}
			logger.info("Out deleteEntrepriseById() without errors.");
		} catch (Exception e) {
			logger.error("Erreur dans deleteEntrepriseById() : ", e);
		}
	}

	@Transactional
	public void deleteDepartementById(int depId) {
		logger.info("In deleteDepartementById() : ");
		logger.debug("DepartementById : {}", depId);
		try {
			Optional<Departement> dep = deptRepoistory.findById(depId);
			logger.debug("Departement: {}", dep);
			if (dep.isPresent()) {
				deptRepoistory.delete(dep.get());
			}
			logger.info("Out deleteDepartementById() without errors.");
		} catch (Exception e) {
			logger.error("Erreur dans deleteDepartementById() : ", e);
		}
	}

	public Entreprise getEntrepriseById(int entrepriseId) {
		logger.info("In getEntrepriseById() : ");
		logger.debug("entrepriseId : {}", entrepriseId);
		try {
			Optional<Entreprise> ent = entrepriseRepoistory.findById(entrepriseId);
			if (ent.isPresent()) {
				logger.debug(ENTRDEB, ent);
				logger.info("Out getEntrepriseById() :");
				return ent.get();
			} else {
				logger.info("Out getEntrepriseById() :");
				return null;
			}
		} catch (Exception e) {
			logger.error("Erreur dans getEntrepriseById() : ", e);
			return null;
		}
	}

}
