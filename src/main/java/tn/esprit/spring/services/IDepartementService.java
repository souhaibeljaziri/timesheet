package tn.esprit.spring.services;

import tn.esprit.spring.entities.Departement;

import java.util.List;


public interface IDepartementService {


    List<Departement> getAllDepartements();


}
