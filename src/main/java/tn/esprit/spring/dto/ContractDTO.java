package tn.esprit.spring.dto;

import java.util.Date;

import tn.esprit.spring.entities.Employe;

public class ContractDTO {

	private int referenceDto;
	private Date dateDebutDto;

	private String typeContratDto;

	private float salaireDto;

	private Employe employeDto;

	public ContractDTO() {
		super();
	}

	public ContractDTO(Date dateDebutDto, String typeContratDto, float salaireDto) {
		this.dateDebutDto = dateDebutDto;
		this.typeContratDto = typeContratDto;
		this.salaireDto = salaireDto;
	}

	public Date getDateDebut() {
		return dateDebutDto;
	}

	public void setDateDebut(Date dateDebutDto) {
		this.dateDebutDto = dateDebutDto;
	}

	public int getReference() {
		return referenceDto;
	}

	public void setReference(int referenceDto) {
		this.referenceDto = referenceDto;
	}

	public String getTypeContrat() {
		return typeContratDto;
	}

	public void setTypeContrat(String typeContratDto) {
		this.typeContratDto = typeContratDto;
	}

	public float getSalaire() {
		return salaireDto;
	}

	public void setSalaire(float salaireDto) {
		this.salaireDto = salaireDto;
	}

	public Employe getEmploye() {
		return employeDto;
	}

	public void setEmploye(Employe employeDto) {
		this.employeDto = employeDto;
	}

}
