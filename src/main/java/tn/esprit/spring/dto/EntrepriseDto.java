package tn.esprit.spring.dto;

import java.util.ArrayList;
import java.util.List;

import tn.esprit.spring.entities.Departement;

public class EntrepriseDto {
	private int id;

	private String name;

	private String raisonSocial;

	private List<Departement> departements = new ArrayList<>();

	public EntrepriseDto() {
		super();
	}

	public EntrepriseDto(String name, String raisonSocial) {
		this.name = name;
		this.raisonSocial = raisonSocial;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRaisonSocial() {
		return raisonSocial;
	}

	public void setRaisonSocial(String raisonSocial) {
		this.raisonSocial = raisonSocial;
	}

	public List<Departement> getDepartements() {
		return departements;
	}

	public void setDepartements(List<Departement> departements) {
		this.departements = departements;
	}
}
