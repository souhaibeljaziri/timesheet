FROM openjdk:8-jdk-alpine
EXPOSE 8083
ADD target/Timesheet-spring-boot-core-data-jpa-mvc-REST-1-1.0.0-SNAPSHOT.jar timesheet-devops-1.0.jar
ENTRYPOINT ["java","-jar","/timesheet-devops-1.0.jar"]